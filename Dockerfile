FROM debian:sid-slim

RUN apt update -y && apt install -y git make libc-dev
RUN ln -sf /usr/local/bin/lld /usr/local/bin/ld
RUN ln -sf /usr/local/bin/llvm-ar /usr/local/bin/ar
RUN ln -sf /usr/local/bin/llvm-as /usr/local/bin/as

ADD rootfs /
