# Docker: llvm/clang/cmake

A docker image containing a full featured C++ LLVM toolchain.

## Using this container image

In order to compile your project using this container, you have to set two environment variables to instruct cmake to properly integrate into the LLVM toolchain.

You can do this in the `variables` section in your `.gitlab-ci.yml` for example:

```yaml
variables:
  CXXFLAGS: --stdlib=libc++
  LINKFLAGS: --rtlib=compiler-rt
```
